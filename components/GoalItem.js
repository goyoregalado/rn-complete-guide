import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';

const GoalItem = ({ id, title, onDelete }) => (
  <TouchableOpacity activeOpacity={0.8} onPress={() => onDelete(id)}>
    <View style={styles.listItem}>
      <Text>{title}</Text>
    </View>
  </TouchableOpacity>
);

/*
  There are other interesting alternatives to TouchableOpacity
  
  TouchableHighlight and you can pass a color with underlayColor prop.

  TouchableNativeFeedback It adapts to native look and feel.

  TouchableNotFeedback
*/

export default GoalItem;

const styles = StyleSheet.create({
  listItem: {
    padding: 10,
    marginVertical: 10,
    backgroundColor: '#ccc',
    borderColor: 'black',
    borderWidth: 1
  }
})